<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMenu extends Model
{
    use HasFactory;
    protected $table = 'user_menu';

    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
