<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;
    protected $table = 'menu';

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsToMany(User::class, UserMenu::class);
    }


    static function validation(): array
    {
        return ['title'=>'required',
        'price'=>'required|numeric',
        'image'=>''];
    }

    static function structure(): array
    {
        return [
            'Image'=>'image',
            'Title'=>'title',
            'Category'=>'category_id',
            'Price'=>'price',
        ];
    }
}
