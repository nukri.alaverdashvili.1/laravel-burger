<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use App\Models\UserMenu;
use App\Models\User;

class MenuController extends Controller
{    
    public function show()
    {
        $menu = Menu::get();
        return view('menu',['menu'=>$menu]);
    }

    public function showCategory($category)
    {
        $category = Category::where('category_name', $category)->get();
        $category = $category->loadMissing('menu');
        return view('category', ['category'=>$category]);
    }

    public function addBasket($id)
    {
        $userId = Auth::id();
        if(empty($userId))
        {
            return redirect('show');
        }
        $checkMenu = UserMenu::where('menu_id', $id)->where('user_id', $userId)->get();
        if($checkMenu->isEmpty())
        {
            $usermenu = new UserMenu;
            $usermenu->menu_id = $id;
            $usermenu->user_id = $userId;
            $usermenu->save();
        }
        
        return redirect('show');
    }

    public function basket()
    {
       $userId = Auth::id();
       $user = User::where('id', $userId)->first();
       if(empty($user)){return redirect('show');}
       $user = $user->loadMissing('menu.category');
       if($user->menu->isEmpty()){return redirect('show');}
       $structure = Menu::structure();
       $totalPrice =0;
       return view('basket',['user'=>$user, 'structure'=>$structure, 'totalPrice'=>$totalPrice]);
    }

    public function removebasket($id)
    {
        $userId = Auth::id();
        $removeFromBasket = UserMenu::where('menu_id', $id)->where('user_id',$userId)->first();
        $removeFromBasket->delete();
        return redirect('basket');
    }

    public function search(Request $request)
    {
        $menu = Menu::where('title', 'like', $request->search.'%')->get();

        return view('menu',['menu'=>$menu]);
    }

}
