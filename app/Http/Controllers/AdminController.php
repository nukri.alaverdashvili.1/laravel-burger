<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Menu;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    public function create()
    {
        $this->authorize('create');
        $category = Category::get();
        return view('create',['category'=>$category]);  
    }
    public function add(Request $request)
    {
        $this->authorize('create');
        $validation = Menu::validation();
        $request->validate($validation);
        $structure = Menu::structure();
        $menu = new Menu();
        foreach ($structure as $name)
        {
            if($name == 'image')
            {
                if($request->file('image') != null)
                {
                    $imagePath = $request->file('image')->store('uploads', 'public');
                    $image = Image::make(public_path("storage/{$imagePath}"))->fit(400,300);
                    $image->save();
                    $menu->$name = $imagePath;
                }
            }else{
                $menu->$name = $request->$name;
            }
        }
        $menu->save();
        return redirect('/');
    }

    public function delete($id)
    {
        $menu = Menu::findOrFail($id);
        $menu->delete();
        return redirect('show');
    }

    public function edit($id)
    {
        $menu = Menu::findOrFail($id);
        $category = Category::all();
        $menu->loadMissing('category');
        return view('edit', ['menu'=>$menu, 'category'=>$category]);
    }

    public function update(Request $request)
    {
        $update = Menu::findOrFail($request->id);
        $validation = Menu::validation();
        $structure = Menu::structure();
        $request->validate($validation);
        foreach($structure as $name)
        {
            if($name == 'image')
            {
                if($request->file('image') != null)
                {
                    $imagePath = $request->file('image')->store('uploads', 'public');
                    $image = Image::make(public_path("storage/{$imagePath}"))->fit(400,300);
                    $image->save();
                    $update->$name = $imagePath;
                }
            }else{
                $update->$name = $request->$name;
            }
        }
        
        $update->save();
        return redirect('show');
    }
}
