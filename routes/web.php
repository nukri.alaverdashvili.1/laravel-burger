<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\MenuController;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('index');
});
Route::get('show', 'MenuController@show');
Route::get('show/{category}', 'MenuController@showCategory');
Route::get('addbasket/{id}', 'MenuController@addBasket')->middleware('verified');
Route::get('basket', 'MenuController@basket')->middleware('verified');
Route::get('removebasket/{id}', 'MenuController@removebasket')->middleware('verified');
Route::post('search', 'MenuController@search');

Route::get('create', 'AdminController@create');
Route::post('create', 'AdminController@add');
Route::get('edit/{id}','AdminController@edit');
Route::post('update','AdminController@update');
Route::get('delete/{id}', 'AdminController@delete');

Auth::routes(['verify'=>true]);





