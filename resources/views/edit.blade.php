@extends('layouts.app')

    @section('content')
    <div class="container">
        @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $f)
    
                <div class="alert alert-danger" role="alert">
                    {{ $f }}
                </div>
            @endforeach
        </ul>
        @endif
    
    <form action="/update" method="post" enctype="multipart/form-data">
        @csrf   
        <br><br>
        <img class="editimg" style="max-width: 320px;" src="/storage/{{$menu->image}}">
        <br><br>
        <div class="input-group mb-3">
            <input type="file" class="form-control" name="image">
        </div>

        <div class="mb-3">
            <label class="form-label">Title</label>
            <input type="text" name="title" value="{{$menu->title}}" class="form-control">
        </div>
        <div class="mb-3">
            <label class="form-label">Price</label>
            <input type="nummer" name="price" value="{{$menu->price}}" class="form-control">
        </div>
    
        <div class="mb-3">
            <select name="category_id">
                <option selected value="{{$menu->category['id']}}">{{$menu->category['category_name']}}</option>
                @foreach ($category as $category_v)
                @if($category_v->id == $menu->category['id'])
                    @continue;
                @endif
            <option value="{{$category_v->id}}">{{$category_v->category_name}}</option>
            @endforeach
          </select>
         </div>

    
        <br><br>
        <input type="hidden" name="id" value="{{$menu->id}}" class="form-control">
        <button type="submit" class="btn btn-primary">Submit</button>
    
    </form>
    

    </div>
    @endsection
