@extends('layouts.app')

    @section('content')
    <div class="container">

      @if ($menu->isEmpty())

      @endif
        @foreach ($menu as $menuitem)
          <div class="card mb-3" style="max-width: 840px;">
            <div class="row g-0">
              <div class="col-md-4">
                <img style="height: 240px;" src="/storage/{{$menuitem->image}}" class="img-fluid rounded-start" alt="...">
              </div>
              <div class="col-md-8">
                <div class="card-body">
                  <h5 class="card-title">{{$menuitem->title}}</h5>
                  <p class="card-text">{{ $menuitem->price }} €</p>
                  @can('create')
                  <td> <a href="edit/{{ $menuitem->id}}" type="button"
                    class="btn btn-outline-primary">Edit</a>
                   </td>                 
                  <td> <a href="delete/{{ $menuitem->id}}" type="button"
                    class="btn btn-outline-primary">Delete</a>
                   </td>
                  @endcan
                  <br>
                  <td><a href="/addbasket/{{$menuitem->id}}" style="margin-top:10px;" type="button"
                    class="btn btn-outline-secondary">Add In Basket</a>
                   </td>
                </div>
              </div>
            </div>
          </div>

        @endforeach


    </div>
    @endsection
