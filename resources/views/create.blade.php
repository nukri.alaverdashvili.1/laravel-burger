@extends('layouts.app')

    @section('content')
    <div class="container">
        
        @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $f)
    
                <div class="alert alert-danger" role="alert">
                    {{ $f }}
                </div>
            @endforeach
        </ul>
        @endif
    
    <form action="create" method="post" enctype="multipart/form-data">
        @csrf   
        <div class="mb-3">
                <select name="category_id">
              @foreach ($category as $category_v)
                <option value="{{$category_v->id}}">{{$category_v->category_name}}</option>
                @endforeach
              </select>
        </div>
        <div class="mb-3">
            <label class="form-label">Title</label>
            <input type="text" name="title" class="form-control">
        </div>
        <div class="mb-3">
            <label class="form-label">Price</label>
            <input type="nummer" name="price" class="form-control">
        </div>
    
        <div class="input-group mb-3">
            <input type="file" class="form-control" name="image">
            <label class="input-group-text" for="inputGroupFile02">Image</label>
        </div>
        <br><br>

        <button type="submit" class="btn btn-primary">Submit</button>
    
    </form>
    

    </div>
    @endsection
