@php
    use Illuminate\Support\Facades\Auth;
    use App\Models\UserMenu;
    try {
        $id = Auth::id();
        $checkMenu = UserMenu::where('user_id', $id)->count();
    } catch (\Throwable $th) {
        $checkMenu=0;
    }
@endphp

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Burger House</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="nav-link" href="/"> <img width="100px" src="/storage/images/logo.png"> </a>
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li>
                        <a class="nav-link" href="/"> Home Page </a>
                    </li>
                    <li>
                        <a class="nav-link" href="/show"> Full Menu</a>
                    </li>
                    <li>
                        <a class="nav-link" href="/show/breakfast"> Breakfast </a>
                    </li>
                    <li>
                        <a class="nav-link" href="/show/lunch"> Lunch </a>
                    </li>
                    <li>
                        <a class="nav-link" href="/show/shakes"> Shakes </a>
                    </li>
                    <li>
                        <a class="nav-link" href="/show/dinner"> Dinner </a>
                    </li>

                    @can('create')
                        <li>
                            <a class="nav-link" href="/create"> Create New Menu </a>
                        </li>
                    @endcan
                    
                </ul>

                <!-- Right Side Of Navbar -->
                    <a style="color: inherit;" class="nav-link" href="/basket"> <img width="40px" src="/storage/images/basket.jpg">{{$checkMenu}}</a>

                    {{-- <form action="/search" class="d-flex" method="post">
                        @csrf
                        <input class="form-control me-2" name="search" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success" type="submit">Search</button>
                    </form> --}}

                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    <li class="nav-item">
                        <form action="/search" class="d-flex" method="post">
                            @csrf
                            <input class="form-control me-2" name="search" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success" type="submit">Search</button>
                        </form>
                    </li>
            
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
    </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
    </div>
</body>

</html>
