@extends('layouts.app')

    @section('content')
    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    @foreach ($structure as $structure_key => $structure_item)
                        <th scope="col">{{$structure_key}}</th>
                        @endforeach
                        <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @foreach ($user->menu as $menu_value)
                    @foreach ($structure as $structure_item)
                        @if($structure_item == 'image')
                        <td><img style="max-width: 100px;" class="rounded-circle w-100" src="/storage/{{$menu_value->$structure_item}}" class="img-fluid rounded-start" alt="..."> </td>
                        @elseif($structure_item =='category_id')
                        <td>{{$menu_value->category['category_name']}}</td>
                        @elseif($structure_item == 'price')
                        <td>{{$menu_value->$structure_item}}</td>
                        @php
                            $totalPrice = $totalPrice + $menu_value->$structure_item;
                        @endphp                      
                        @else
                        <td>{{$menu_value->$structure_item}}</td>
                        @endif
                    @endforeach
                    <td><a href="/removebasket/{{$menu_value['id']}}" style="margin-top:10px;" type="button"
                        class="btn btn-outline-secondary">Remove From Basket</a>
                       </td>
                </tr>
                @endforeach

            </tbody>
          </table>
            <div class="card-body">
              <p class="card-text">Total Price Is : {{$totalPrice}}</p>
              <td><a href="/" style="margin-top:10px;" type="button"
                  class="btn btn-outline-secondary">Pay</a>
                 </td>
            </div>

    </div>
    @endsection
