<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->delete();
        DB::table('menu')->insert(array(
            0=>array(
                'title' => "Buttermilk Pancakes",
                'category_id'=>1,
                'price'=>15.99,
                'image'=>'images/item1.jpg',
            ),
            1=>array(
                'title' => "diner double",
                'category_id'=>2,
                'price'=>13.99,
                'image'=>'images/item2.jpg',
            ),
            2=>array(
                'title' => "oreo dream",
                'category_id'=>3,
                'price'=>18.99,
                'image'=>'images/item3.jpg',
            )
            ));

    }
}
