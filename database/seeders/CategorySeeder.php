<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->delete();
        DB::table('category')->insert(array(
            0=>array(
                'category_name' => "breakfast",
            ),
            1=>array(
                'category_name' => "lunch",
            ),
            2=>array(
                'category_name' => "shakes",
            ),
            3=>array(
                'category_name' => "dinner",
            )
            ));

    }
}
